/*
 * Creates a notice box (like an Android toast message). Notices should only be used to show information to the 
 * user where user feedback-interaction is not needed (e.g. "invalid passowrd" or "save successful" 
 * or "you have a new message"). 
 * 
 * Notices can have an icon, a message and/or a contextual status accosiated with them.
 * statuses include:
 * 		success
 * 		error
 * 		warn
 *		info
 * Any other status name will default to nothing and the default theme persists. 
 *
 * Icons are passed in as class name using icons from FontAwesome (e.g. 'fa-comments'). 
 * Message is a string of textual information for the user, it should be short and to the point.
 *
 * For notifications that require or allow for user feedback (e.g. "successfully deleted life | undo")
 * please use Alert (works like Android Snackbar).
 * 
 */

var notice = {
	windowLoad: function () {
		$("body").on("click", ".notification__toggle", notice.show );

		$("body").append("<div class='notice hide' />");
		$(".notice").append("<div class='notice__icon' />");
			$(".notice__icon").append("<span class='fa' />");
		$(".notice").append("<div class='notice__message' />");
	},
	show: function (icon, message, status) {
		icon = typeof icon !=='undefined' ? icon : 'fa-info-circle';
		message = typeof message !=='undefined' ? message : 'Default message: No message provided.';
		status = typeof status !=='undefined' ? status : "default";

		switch (status) {
			case 'success' :
				$(".notice").addClass("success");
				break;
			case 'error' :
				$(".notice").addClass("error");
				break;
			case 'warn' :
				$(".notice").addClass("warn");
				break;
			case 'info' :
				$(".notice").addClass("info");
				break;
			case 'default':
			default:
				break;
		}

		$(".notice__icon .fa").addClass(icon);
		$(".notice__message").append("<pre>" + message + "</pre>");
 
		$(".notice").toggleClass("hide");
		setTimeout(
			function() {
				notice.hide(icon, message, status);
			}, 3000);
		return;
	},
	hide: function(icon, message, status) {
		$(".notice").removeClass(status);
		$(".notice__icon .fa").removeClass(icon);
		$(".notice__message pre").remove();
		$(".notice").toggleClass("hide");
	}
}

var alert = {
	windowLoad: function () {
		$("body").on("click", ".alert__toggle", alert.show );

		$("body").append("<div class='alert hide' />");
		$(".alert").append("<div class='alert__icon' />");
			$(".alert__icon").append("<span class='fa' />");
		$(".alert").append("<div class='alert__message' />");
		$(".alert").append("<div class='alert__action' />");
		$(".alert__action").text("UNDO");
	},
	show: function (icon, message, status) {
		icon = typeof icon !=='undefined' ? icon : 'fa-info-circle';
		message = typeof message !=='undefined' ? message : 'Default message: No message provided.';
		status = typeof status !=='undefined' ? status : "default";

		switch (status) {
			case 'success' :
				$(".alert").addClass("success");
				break;
			case 'error' :
				$(".alert").addClass("error");
				break;
			case 'warn' :
				$(".alert").addClass("warn");
				break;
			case 'info' :
				$(".alert").addClass("info");
				break;
			case 'default':
			default:
				break;
		}

		$(".alert__icon .fa").addClass(icon);
		$(".alert__message").append("<pre>" + message + " </pre>");
 
		$(".alert").toggleClass("hide");
		setTimeout(
			function() {
				alert.hide(icon, message, status);
			}, 6000);
		return;
	},
	hide: function(icon, message, status) {
		$(".alert").removeClass(status);
		$(".alert__icon .fa").removeClass(icon);
		$(".alert__message pre").remove();
		$(".alert").toggleClass("hide");
	}
}