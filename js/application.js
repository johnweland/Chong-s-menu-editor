$(document).ready(function() {
	notice.windowLoad();
	alert.windowLoad();


	var table = $('#menu-items').DataTable({
		"sort": true,
    "filter": true,
    "paginate": true,
    "lengthChange": false,
    "pageLength": 15,
    "autoWidth": false,
    "ajax": "data/dummydata.json",
    "order": [[6, "asc" ]],
    "columnDefs" : [],
    "columns": [
    		{"data": "number", "sClass" : "number-column", "title" : "Number" },
        {"data": "name", "sClass" : "name-column", "title" : "Name" },
        {"data": "price", "sClass": "price-column", "title" : "Price"},
        {"data": "description", "sClass": "description-column", "title": "Description"},
        {"data": "hangul", "sClass": "hangul-column", "title": "Hangul"},
        {"data": "image_url", "sClass": "image-url-column", "title": "Image URL"},
        {"data": "category", "sClass": "category-column", "title": "Category(s)"}
    ],
    "fnRowCallback": formatRow,
    "language": {
        "emptyTable": "Empty Table",
        "loadingRecords": "Loading Records...",
        "proccessing": "Processing...",
        "zeroRecords": "No Records Found"
    },
    "drawCallback": documentReady
	})
});

function formatRow(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
	aData['price'] = parseFloat(aData['price']).toFixed(2);
	$(nRow).find('.price-column').text("$" + aData['price']);
	aData['number'] = parseInt(aData['number']);
	if (aData['category'] == "Special Dishes") {
		$(nRow).find('.number-column').text("S" + aData['number']);
	}

}

function documentReady() {

}